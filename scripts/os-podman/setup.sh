SUPPORTED_DEBIAN_VERSIONS=(10)
SUPPORTED_UBUNTU_VERSIONS=(19.10 19.04 18.04)
SUPPORTED_RASPBIAN_VERSIONS=(10)

function install_dependencies() {
	# Put your global dependencies like wget/python/php in here
	echo "Installing dependencies..."
	apt-get -qq -y install wget gnupg
}

function install_debian() {
	# Beginning of installation script
	require_root true
	version="$1"
	echo "Installing for Debian $version..."

	# [OPTIONAL] Version specific branching
	# -

	echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /' >/etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
	wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Debian_10/Release.key -O- | apt-key add -

	apt-get update -qq
	apt-get -qq -y install podman
}

function install_ubuntu() {
	# Beginning of installation script
	require_root true
	version="$1"
	echo "Installing for Ubuntu $version..."

	# [OPTIONAL] Version specific branching
	# -

	sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${version}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
	wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_${version}/Release.key -O- | apt-key add -
	apt-get update -qq
	apt-get -qq -y install podman
}

function install_raspbian() {
	# Beginning of installation script
	require_root true
	version="$1"
	echo "Installing for Raspbian $version..."

	# [OPTIONAL] Version specific branching
	# -

	# Installations steps from pdoman.io
	echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Raspbian_10/ /' >/etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
	wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Raspbian_10/Release.key -O- | apt-key add -
	apt-get update -qq
	apt-get -qq -y install podman
}

function pre_setup_check_and_branch() {
	# Determine current OS, including the version.
	# Branch into the target specific installation script if
	# a supported version has been found.
	source /etc/os-release
	if [ "$ID" == "debian" ]; then
		for i in "${SUPPORTED_DEBIAN_VERSIONS[@]}"; do
			if [ "$i" == "$VERSION_ID" ]; then
				install_dependencies
				install_debian "$i"
				exit 0
			fi
		done
		echo "There is no installation script for $ID with version $VERSION_ID"
		exit 2
	elif [ "$ID" == "ubuntu" ]; then
		for i in "${SUPPORTED_UBUNTU_VERSIONS[@]}"; do
			if [ "$i" == "$VERSION_ID" ]; then
				install_dependencies
				install_ubuntu "$i"
				exit 0
			fi
		done
		echo "There is no installation script for $ID with version $VERSION_ID"
		exit 2
	elif [ "$ID" == "raspbian" ]; then
		for i in "${SUPPORTED_RASPBIAN_VERSIONS[@]}"; do
			if [ "$i" == "$VERSION_ID" ]; then
				install_dependencies
				install_raspbian "$i"
				exit 0
			fi
		done
		echo "There is no installation script for $ID with version $VERSION_ID"
		exit 2
	else
		echo "There is no installation script for $ID"
		exit 1
	fi
}

function require_root() {
	if [ "$1" == "true" ]; then
		if [ ! "$(whoami)" == "root" ]; then
			echo "You need to be root to execute this script! Aborting..."
			exit 3
		fi
	fi

}

pre_setup_check_and_branch "$@"
