# RPi AirPlay

Rasperry Pi to Apple AirPlay Module

## Important

Option -a auf "hdmi"
Option -b immer auf off, damit wird wieder das Terminal des Pis angezeigt wenn disconnected wird.

```
 ./rpiplay -n 0x30 -a hdmi -b off
 ```

## Installing


```
git clone https://github.com/FD-/RPiPlay.git


cd RPiPlay

sudo apt-get install cmake
sudo apt-get install libavahi-compat-libdnssd-dev
sudo apt-get install libssl-dev
mkdir build
cd build
cmake ..
make
```

## Usage

Start the rpiplay executable and an AirPlay mirror target device will appear in the network. At the moment, these options are implemented:

-n name: Specify the network name of the AirPlay server.

-b (on|auto|off): Show black background always, only during active connection, or never.

-r (90|180|270): Specify image rotation in multiples of 90 degrees.

-l: Enables low-latency mode. Low-latency mode reduces latency by effectively rendering audio and video frames as soon as they are received, ignoring the associated timestamps. As a side effect, playback will be choppy and audio-video sync will be noticably off.

-a (hdmi|analog|off): Set audio output device

-d: Enables debug logging. Will lead to choppy playback due to heavy console output.

-v/-h: Displays short help and version information.

## Source

https://github.com/FD-/RPiPlay
